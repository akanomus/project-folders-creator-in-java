package cz.aricoma.ProjectFoldersCreator;

import cz.aricoma.ProjectFoldersCreator.console.ConsoleFacade;
import cz.aricoma.ProjectFoldersCreator.exception.WrongKeyException;
import cz.aricoma.ProjectFoldersCreator.file.FileFacade;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class ProjectFoldersCreatorApplication {

    private static final FileFacade folderCtrl = new FileFacade();
    private static final ConsoleFacade consoleCtrl = new ConsoleFacade();

    public static void main(String[] args) {
        SpringApplication.run(ProjectFoldersCreatorApplication.class, args);
        run();
    }

    private static void run() {
        consoleCtrl.printInitialMessage();

        String key;
        do {
            key = step();
        } while (!key.equals("E"));
    }

    private static String step() {
        String key = "WRONG_ACTION";

        var folderName = folderCtrl.getCurrFolderName();
        var nestedFileNames = folderCtrl.getNestedFileNames();

        consoleCtrl.printFolder(folderName, nestedFileNames);

        try {
            key = consoleCtrl.getKey();
        } catch (WrongKeyException e) {
            return key;
        }

        takeAction(key);

        return key;
    }

    private static void takeAction(String key) {
        switch (key) {
            case "A":
                folderCtrl.createFolder(consoleCtrl.getName());
                break;
            case "D":
                folderCtrl.deleteFile(consoleCtrl.getName());
                break;
            case "P":
                folderCtrl.createProject(consoleCtrl.getName());
                break;
            case "F":
                folderCtrl.createFile(consoleCtrl.getName());
                break;
            case "R":
                folderCtrl.renameFile(consoleCtrl.getName(), consoleCtrl.getNewName());
                break;
            case "M":
                folderCtrl.moveTo(consoleCtrl.getName());
                break;
            case "B":
                folderCtrl.moveBack();
                break;
            case "E":
                break;
        }
    }

}
