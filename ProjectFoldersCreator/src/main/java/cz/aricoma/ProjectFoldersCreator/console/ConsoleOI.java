package cz.aricoma.ProjectFoldersCreator.console;

import java.util.List;
import java.util.Scanner;

public class ConsoleOI {

    private final Scanner scanner = new Scanner(System.in);

    private static final int STANDARD_SHIFT = 4;

    private void printListWithShift(List<String> list, int shift) {
        for (String s : list) {
            for (int i = 0; i < shift; i++) {
                System.out.print("-");
            }
            System.out.print("|  ");
            System.out.println(s);
        }
    }

    private void printNestedFiles(List<String> fileNames) {
        printListWithShift(fileNames, STANDARD_SHIFT);
    }

    public void printFolder(String folder, List<String> nestedFiles) {
        System.out.print("|Current folder: ");
        System.out.println(folder);

        System.out.println("|Nested folders and files:");
        printNestedFiles(nestedFiles);
    }

    public void printKeyToAction(String key, String action) {
        var msg = "\t" + key + " to " + action;
        System.out.println(msg);
    }

    public String getInputLine() {
        return scanner.nextLine();
    }

}
