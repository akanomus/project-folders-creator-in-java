package cz.aricoma.ProjectFoldersCreator.console;

import cz.aricoma.ProjectFoldersCreator.exception.WrongKeyException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class ConsoleFacade {

    private static final Logger logger = LoggerFactory.getLogger(ConsoleFacade.class);

    private final ConsoleOI consoleOI = new ConsoleOI();

    private final Map<String, String> keyToAction = new LinkedHashMap<>() {{
        put("A", "add folder");
        put("D", "delete folder or file");
        put("P", "add project folder");
        put("F", "add file");
        put("R", "rename folder or file");
        put("M", "move to folder");
        put("B", "move back");
        put("E", "exit");
    }};

    public void printInitialMessage() {
        System.out.println();
        System.out.println("Welcome to project folders creator application!");
        System.out.println("|---------------------------------------------|");
    }

    public void printFolder(String folder, List<String> nestedFiles) {
        consoleOI.printFolder(folder, nestedFiles);
        System.out.println();
    }

    public void printKeys() {
        System.out.println("Enter");
        keyToAction.forEach(consoleOI::printKeyToAction);
    }

    public String getKey() {
        this.printKeys();
        var key = consoleOI.getInputLine().toUpperCase();

        if (!this.keyToAction.containsKey(key)) {
            logger.error("Wrong input. Action {} does not exist!", key);
            throw new WrongKeyException();
        }

        return key;
    }

    public String getName() {
        System.out.println("Enter the folder or file name.");
        return consoleOI.getInputLine();
    }

    public String getNewName() {
        System.out.println("Enter the new name.");
        return consoleOI.getInputLine();
    }

}
