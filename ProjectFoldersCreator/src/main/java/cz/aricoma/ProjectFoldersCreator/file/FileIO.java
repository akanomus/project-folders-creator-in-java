package cz.aricoma.ProjectFoldersCreator.file;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class FileIO {

    private static final Logger logger = LoggerFactory.getLogger(FileIO.class);

    public boolean isFolder(String path) {
        var file = new File(path);
        return file.isDirectory();
    }

    public boolean fileExists(String path) {
        var file = new File(path);
        return file.exists();
    }

    public File getFile(String path) {
        if (!fileExists(path)) {
            logger.error("Folder or file {} does not exist.", path);
            return null;
        }
        return new File(path);
    }

    public List<String> readFile(String path) {
        List<String> lines = null;

        try {
            lines = Files.readAllLines(Paths.get(path));
        } catch (IOException e) {
            logger.error("Could not read the file.");
            return lines;
        }

        if (lines.size() == 0) {
            logger.error("File is empty.");
        }
        return lines;
    }

    public void createFile(String path) {
        var file = new File(path);
        try {
            if (!file.createNewFile()) {
                logger.error("File {} already exist.", path);
                return;
            }
        } catch (IOException e) {
            logger.error("Couldn't create a file.");
            return;
        }

        logger.info("Created: " + path);
    }

    public void createFolder(String path) {
        if (fileExists(path)) {
            logger.error("Folder {} already exist.", path);
            return;
        }

        var folder = new File(path);
        if (!folder.mkdir()) {
            logger.error("Couldn't create a folder.");
            return;
        }

        logger.info("Created: " + path);
    }

    private void deleteFile(String path) {
        var folder = this.getFile(path);
        if (folder == null) {
            return;
        }

        if (!folder.delete()) {
            logger.error("Couldn't delete a folder.");
            return;
        }
        logger.info("Deleted: " + path);
    }

    public void deleteFiles(String path) {
        var nestedFiles = getNestedFiles(path);

        if (nestedFiles == null) {
            return;
        }

        if (nestedFiles.size() == 0) {
            deleteFile(path);
            return;
        }

        nestedFiles.forEach(file -> deleteFiles(file.getPath()));
        deleteFile(path);
    }

    public void renameFile(String path, String newPath) {
        var oldFolder = this.getFile(path);
        if (oldFolder == null) {
            return;
        }

        if (fileExists(newPath)) {
            logger.error("Folder {} already exist.", newPath);
            return;
        }

        var newFolder = new File(newPath);
        if (!oldFolder.renameTo(newFolder)){
            logger.error("Could not rename the folder.");
        }
    }

    public List<File> getNestedFiles(String path) {
        if (!fileExists(path)) {
            logger.error("Folder {} does not exist.", path);
            return null;
        }

        if (!isFolder(path)) {
            return new ArrayList<>();
        }

        var folder = new File(path);
        var files = folder.listFiles();

        if (files == null) {
            logger.error("IO error.");
            return null;
        }

        return Arrays.asList(files);
    }

    public List<String> getNestedFileNames(String path) {
        return getNestedFiles(path)
                .stream()
                .map(File::getName)
                .collect(Collectors.toList());
    }

}
