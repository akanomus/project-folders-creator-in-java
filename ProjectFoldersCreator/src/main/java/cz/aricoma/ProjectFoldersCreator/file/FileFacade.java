package cz.aricoma.ProjectFoldersCreator.file;

import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.List;

public class FileFacade {

    private static final Logger logger = LoggerFactory.getLogger(FileFacade.class);

    @Getter
    private static final String BASE_FOLDER_PATH = "SharedDisk:\\projects";

    @Getter
    private static final String TEMPLATE_PATH = "src\\main\\resources\\projectTemplate.txt";

    private final FileIO fileIO = new FileIO();
    private File currFolder = fileIO.getFile(BASE_FOLDER_PATH);


    public String getCurrFolderName() {
        return this.currFolder.getName();
    }

    private boolean isFolder(File file) {
        if (!fileIO.isFolder(file.getPath())) {
            logger.error("{} is not a folder", file.getName());
            return false;
        }
        return true;
    }

    public List<String> getNestedFileNames() {
        return fileIO.getNestedFileNames(this.currFolder.getPath());
    }

    private void setCurrentFolder(File file) {
        this.currFolder = file;
    }

    public void moveTo(String name) {
        var folder = fileIO.getFile(getNestedFilePath(name));

        if (folder == null) {
            return;
        }

        if (!isFolder(folder)) {
            return;
        }

        this.setCurrentFolder(folder);
    }

    private boolean inBaseFolder() {
        return this.currFolder.getPath().equals(BASE_FOLDER_PATH);
    }

    public void moveBack() {
        if (inBaseFolder()) {
            logger.error("You are located in base folder.");
            return;
        }

        var path = this.currFolder.getParent();
        if (path == null) {
            logger.error("Parent folder not found. Moving back to base folder.");
            this.setCurrentFolder(fileIO.getFile(BASE_FOLDER_PATH));
        }

        var parent = fileIO.getFile(path);
        this.setCurrentFolder(parent);
    }

    private String getNestedFilePath(String name) {
        return this.currFolder + "\\" + name;
    }

    public void createFile(String name) {
        if (inBaseFolder()) {
            logger.error("Cannot create a file in base folder.");
            return;
        }
        fileIO.createFile(getNestedFilePath(name));
    }

    private void createProjectFolder(String name) {
        fileIO.createFolder(BASE_FOLDER_PATH + "\\" + name);
    }

    public void createFolder(String name) {
        if (inBaseFolder()) {
            logger.error("Cannot create a folder in base folder.");
            return;
        }
        fileIO.createFolder(getNestedFilePath(name));
    }

    public void deleteFile(String name) {
        fileIO.deleteFiles(getNestedFilePath(name));
    }

    public void renameFile(String currentName, String newName) {
        fileIO.renameFile(getNestedFilePath(currentName), getNestedFilePath(newName));
    }



    public void createProject(String name) {
        if (!inBaseFolder()) {
            logger.error("Cannot create a project anywhere except base folder.");
            return;
        }

        this.createProjectFolder(name);
        this.moveTo(name);

        var template = fileIO.readFile(TEMPLATE_PATH);
        if (template == null) {
            logger.error("Template error.");
            return;
        }

        var nestedFilePaths = TemplateParser.parseTemplateToPath(template);

        nestedFilePaths
                .forEach(this::createFolder);
        this.moveBack();
    }

}
