package cz.aricoma.ProjectFoldersCreator;

import cz.aricoma.ProjectFoldersCreator.file.FileFacade;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;

@SpringBootTest
public class FileFacadeTests {

    private static final FileFacade fc = new FileFacade();
    private static final String projectName = "New project";

    @BeforeAll
    public static void setUpProject() {
        fc.createProject(projectName);
    }

    @AfterAll
    public static void destroyProject()  {
        fc.deleteFile(projectName);
    }

    @Test
    public void createProjectGeneratesProjectTest() {
        var project = getProject();
        assertTrue(project.exists());
    }

    @Test
    public void createProjectGeneratesFolderTest() {
        var project = getProject();
        assertTrue(project.isDirectory());
    }

    @Test
    public void createProjectNameTest() {
        var project = getProject();
        assertEquals(projectName, project.getName());
    }

    @Test
    public void createProjectGeneratesSubFoldersTest() {
        var project = getProject();
        var subFolders = project.listFiles();
        assertTrue(subFolders.length > 0);
    }

    private File getProject() {
        return new File(FileFacade.getBASE_FOLDER_PATH() + "\\" + projectName);
    }

}
